package org.nrg.xnat.generated.plugin;

import org.nrg.framework.annotations.XnatDataModel;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(value = "image-push", name = "TIP Image Push Plugin", description = "Pushes processed image data back to originating PACS systems.")
public class TipImagePushPlugin {
}